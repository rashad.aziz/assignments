package assignments.assignment3;

class Makanan {

    // class attributes declaration

    private String nama;

    private long harga;

    // constructor
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }
    // override toString() to return name when printed
    public String toString() {
        return nama;
    }
    // getters and setters
    public long getHarga() {
        return harga;
    }
}