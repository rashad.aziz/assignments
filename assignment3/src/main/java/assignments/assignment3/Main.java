package assignments.assignment3;

import java.util.Scanner;

public class Main {

    // Class attributes declaration

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    static int totalMataKuliah = 0;

    static int totalElemenFasilkom = 0;

    // method to get ElemenFasilkom object based on their name
    static ElemenFasilkom getPerson(String objek) {
        ElemenFasilkom person = null;
        for (ElemenFasilkom elemen: daftarElemenFasilkom) {
            if (elemen != null) {
                if (elemen.toString().equals(objek)) {
                    person = elemen;
                    break;
                }
            }
        }
        return person;
    }
    // method to get MataKuliah object based on it's name
    static MataKuliah getMatkul(String objek) {
        MataKuliah matkul = null;
        for (MataKuliah course: daftarMataKuliah) {
            if (course != null) {
                if (course.toString().equals(objek)) {
                    matkul = course;
                    break;
                }
            }
        }
        return matkul;
    }
    // this method will append the new student to the array
    static void addMahasiswa(String nama, long npm) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;
    }
    // this method will append the new dosen to the array
    static void addDosen(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;
    }
    // this method will append the new elemen kantin to the array
    static void addElemenKantin(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        System.out.println(nama + " berhasil ditambahkan");
        totalElemenFasilkom++;
    }
    // method to handle menyapa query
    static void menyapa(String objek1, String objek2) {
        // will get the people in the query
        ElemenFasilkom person1 = getPerson(objek1), person2 = getPerson(objek2);
        // if the people are the same object
        if (person1 == person2) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } // if person1 has greeted person2
        else if (person1.telahMenyapa(person2)) {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", objek1, objek2);
        } else {
            person1.menyapa(person2);
            person2.menyapa(person1);
            // handle case if dosen and student greets each other
            System.out.printf("%s menyapa dengan %s\n", objek1, objek2);
            if (person2.getTipe().equals("Dosen") && person1.getTipe().equals("Mahasiswa")) {
                Dosen dosen = (Dosen) person2;
                Mahasiswa mahasiswa = (Mahasiswa) person1;
                if (mahasiswa.hasMatkul(dosen.getMataKuliah())) {
                    person1.setFriendship(2);
                    person2.setFriendship(2);
                }
            } else if (person1.getTipe().equals("Dosen") && person2.getTipe().equals("Mahasiswa")) {
                Dosen dosen = (Dosen) person1;
                Mahasiswa mahasiswa = (Mahasiswa) person2;
                if (mahasiswa.hasMatkul(dosen.getMataKuliah())) {
                    person1.setFriendship(2);
                    person2.setFriendship(2);
                }
            }
        }
    }
    // will handle add makanan query
    static void addMakanan(String objek, String namaMakanan, long harga) {
        // will get the Elemen Fasilkom
        ElemenFasilkom person = getPerson(objek);
        // if the person is not Elemen Kantin
        if (!person.getTipe().equals("Kantin")) {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
        } else { // this will add the food
            ElemenKantin kantin = (ElemenKantin) person;
            kantin.setMakanan(namaMakanan, harga);
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", objek, namaMakanan, harga);
        }
    }
    // will handle beli makanan query
    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        // get the person in the query
        ElemenFasilkom person1 = getPerson(objek1);
        ElemenFasilkom person2 = getPerson(objek2);
        // seller has to be from Elemen Kantin
        if (!person2.getTipe().equals("Kantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        } // seller cannot but their own food
        else if (person2.getTipe().equals("Kantin") && (person1 == person2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else {
            person1.membeliMakanan(person1, person2, namaMakanan);
        }
    }
    // will handle create matkul query
    static void createMatkul(String nama, int kapasitas) {
        // append new mata kuliah to course array
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
        totalMataKuliah++;
    }
    // will handle add matkul query
    static void addMatkul(String objek, String namaMataKuliah) {
        // get the student who wants to enroll
        ElemenFasilkom person = getPerson(objek);
        // get the course
        MataKuliah course = getMatkul(namaMataKuliah);
        // person has to be a student/mahasiswa
        if (!person.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        } else {
            Mahasiswa student = (Mahasiswa) person;
            // cannot enroll to a course that the student already enrolled to before
            if (student.hasMatkul(course)) {
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", namaMataKuliah);
            } // if the course is full
            else if (course.hasNoSpace()) {
                System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", namaMataKuliah);
            } else {
                student.addMatkul(course);
                course.addMahasiswa(student);
                System.out.printf("%s berhasil menambahkan mata kuliah %s\n", objek, namaMataKuliah);
            }
        }
    }
    // will handle drop matkul query
    static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom person = getPerson(objek);
        if (!person.getTipe().equals("Mahasiswa")) {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        } else {
            MataKuliah matkul = getMatkul(namaMataKuliah);
            Mahasiswa student = (Mahasiswa) person;
            // if the student has not taken the course before
            if (!student.hasMatkul(matkul)) {
             System.out.printf("[DITOLAK] %s belum pernah diambil\n", namaMataKuliah);
         } else {
             student.dropMatkul(matkul);
             matkul.dropMahasiswa(student);
             System.out.printf("%s berhasil drop mata kuliah %s\n", objek, namaMataKuliah);
            }
        }
    }
    // will handle query to assign dosen to a course
    static void mengajarMatkul(String objek, String namaMataKuliah) {
        // get the dosen
        ElemenFasilkom person = getPerson(objek);
        MataKuliah course = getMatkul(namaMataKuliah);
        // the person has to be a dosen
        if (!person.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        } else {
            Dosen dosen = (Dosen) person;
            dosen.mengajarMataKuliah(course);
        }

    }
    // will handle query to unassign dosen from course
    static void berhentiMengajar(String objek) {
        ElemenFasilkom person = getPerson(objek);
        if (!person.getTipe().equals("Dosen")) {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        } else {
            Dosen dosen = (Dosen) person;
            dosen.dropMataKuliah();
        }
    }
    // will handle query for student summary
    static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom person = getPerson(objek);
        if (!person.getTipe().equals("Mahasiswa")) {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", objek);
        } else {
            // will print out various attributes related to the student and their course list
            Mahasiswa student = (Mahasiswa) person;
            System.out.println("Nama: " + student);
            System.out.println("Tanggal lahir: " + student.getTanggalLahir());
            System.out.println("Jurusan: " + student.getJurusan());
            System.out.println("Daftar Mata Kuliah:");
            MataKuliah[] daftarMatkul = student.getDaftarMataKuliah();
            if (student.getJumlahMatkul() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                for (int i = 0; i < daftarMatkul.length; i++) {
                    if (daftarMatkul[i] != null) {
                        System.out.println((i + 1) + ". " + daftarMatkul[i]);
                    }
                }
            }
        }
    }
    // will handle query for course summary
    static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah course = getMatkul(namaMataKuliah);
        // will print out various attributes related to the course and the list of students enrolled
        System.out.println("Nama mata kuliah: " + course);
        System.out.println("Jumlah mahasiswa: " + course.getNumOfStudents());
        System.out.println("Kapasitas: " + course.getKapasitas());
        System.out.println("Dosen pengajar: " + ((course.hasDosen())? course.getDosen(): "Belum ada" ));
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        Mahasiswa[] students = course.getDaftarMahasiswa();
        if (course.getNumOfStudents() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            for (int i = 0; i < students.length; i++) {
                if (students[i] != null) {
                    System.out.println((i + 1) + ". " + students[i]);
                }
            }
        }
    }
    // this method will calculate if the person gets +10/-5 friendship
    static void nextDay() {
        for (ElemenFasilkom person: daftarElemenFasilkom) {
            if (person != null) {
                if (person.getJumlahSapa() >= ((totalElemenFasilkom-1) / 2)) {
                    person.setFriendship(10);
                    if (person.getFriendship() > 100) {
                        person.limitFriendship();
                    }
                } else {
                    person.setFriendship(-5);
                    if (person.getFriendship() < 0) {
                        person.limitFriendship();
                    }
                }
                person.resetMenyapa();
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        // call ranking method to rank Elemen Fasilkom based on their friendship rating
        friendshipRanking();
    }
    // this method will rank all Elemen Fasilkom based on their friendship rating
    static void friendshipRanking() {
        // selection sort implementation of ranking
        for (int i = 0; i < totalElemenFasilkom; i++ ) {
            if (daftarElemenFasilkom[i] != null) {
                int max = i;
                for (int j = i; j < totalElemenFasilkom; j++) {
                    if (daftarElemenFasilkom[j].getFriendship() > daftarElemenFasilkom[max].getFriendship()) {
                        max = j;
                    } // if the ratings between 2 people are the same then sort lexicographically
                    else if (daftarElemenFasilkom[j].getFriendship() == daftarElemenFasilkom[max].getFriendship()) {
                        ElemenFasilkom person1 = daftarElemenFasilkom[j], person2 = daftarElemenFasilkom[max];
                        if ((person1.toString().compareTo(person2.toString()) < 0)) {
                            max = j;
                        }
                    }
                }
                // swap
                ElemenFasilkom temp = daftarElemenFasilkom[max];
                daftarElemenFasilkom[max] = daftarElemenFasilkom[i];
                daftarElemenFasilkom[i] = temp;
            }
        } // this will print out the sorted array
        for (int k = 0; k < daftarElemenFasilkom.length; k++) {
            if (daftarElemenFasilkom[k] != null) {
                System.out.printf("%d. %s(%d)\n", k+1, daftarElemenFasilkom[k], daftarElemenFasilkom[k].getFriendship());
            }
        }
    }
    // this method will be called when there is program end query
    static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}