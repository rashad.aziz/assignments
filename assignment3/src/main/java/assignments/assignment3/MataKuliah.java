package assignments.assignment3;

class MataKuliah {

    // class attributes declaration

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    private int numOfStudents;

    // constructor
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }
    // add student method
    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[numOfStudents] = mahasiswa;
        numOfStudents++;
    }
    // drop mahasiswa method
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int index = 0, length = daftarMahasiswa.length;
        // find index of student
        for (int i = 0; i < length; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                index = i;
            }
        } // shift all the elements starting from index
        for (int j = index; j < length-1; j++) {
            daftarMahasiswa[j] = daftarMahasiswa[j+1];
        } // set the last element to null if == to second to last element
        if (daftarMahasiswa[length-1] == daftarMahasiswa[length-2]) {
            daftarMahasiswa[length-1] = null;
        }
        numOfStudents--;
    }
    // verify whether the course has a dosen, and if the course is full or not
    public boolean hasDosen(){
        return dosen != null;
    }
    public boolean hasNoSpace() {
        return numOfStudents == kapasitas;
    }

    // add dosen method
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }
    // method to remove dosen
    public void dropDosen() {
        this.dosen = null;
    }
    public String toString() {
        return nama;
    }
    // getters and setters
    public int getNumOfStudents() {
        return numOfStudents;
    }
    public int getKapasitas() {
        return kapasitas;
    }
    public Dosen getDosen() {
        return dosen;
    }
    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }
}