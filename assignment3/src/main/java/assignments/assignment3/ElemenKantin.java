package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    // class attributes declaration

    private Makanan[] daftarMakanan = new Makanan[10];

    private int numOfFood;

    // constructor
    public ElemenKantin(String nama) {
        this.nama = nama;
        this.tipe = "Kantin";
    }
    // method to add makanan to the elemen kantin's menu
    public void setMakanan(String nama, long harga) {
        if (hasMakanan(nama)) {
            System.out.printf("[DITOLAK] %s sudah pernah terdaftar", nama);
        } else {
            daftarMakanan[numOfFood] = new Makanan(nama, harga);
            numOfFood++;
        }
    }
    // verify whether the elemen kantin has the food or not
    public boolean hasMakanan(String makanan) {
        boolean has = false;
        for (Makanan food: daftarMakanan) {
            if (food != null) {
                if (food.toString().equals(makanan)) {
                    has = true;
                    break;
                }
            }
        }
        return has;
    }
    // setters and getters
    public long getPrice(String makanan) {
        long price = 0;
        for (Makanan eat: daftarMakanan) {
            if (eat.toString().equals(makanan)) {
                price = eat.getHarga();
                break;
            }
        }
        return price;
    }
}
