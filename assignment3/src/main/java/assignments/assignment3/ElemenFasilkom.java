package assignments.assignment3;

// abstract class so it can be used as superclass for multiple subclasses
abstract class ElemenFasilkom {
    
    // class attributes declaration
    // protected variables means that extensions of this class can access the variables
    protected String tipe;
    
    protected String nama;

    private int friendship;

    private int jumlahSapa;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    // this is the method that will be called
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        telahMenyapa[jumlahSapa] = elemenFasilkom;
        jumlahSapa++;
    }
    // reset after next day query
    public void resetMenyapa() {
        telahMenyapa = new ElemenFasilkom[100];
        jumlahSapa = 0;
    }
    // this method will handle beli makanan query
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        // cast Elemen Fasilkom into Elemen Kantin
        ElemenKantin seller = (ElemenKantin) penjual;
        // checks if the seller sells the food
        if (!seller.hasMakanan(namaMakanan)) {
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
        } else {
            System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, namaMakanan,
                    seller.getPrice(namaMakanan));
            pembeli.setFriendship(1);
            seller.setFriendship(1);
        }
    }
    // method to verify whether the person has already greeted the other person
    public boolean telahMenyapa(ElemenFasilkom elemen){
        boolean telah = false;
        for (ElemenFasilkom person: telahMenyapa) {
            if (person == elemen) {
                telah = true;
                break;
            }
        }
        return telah;
    }
    // will limit frienship so 0 <= friendship rating <= 100
    public void limitFriendship() {
        if (friendship > 100) {
            friendship = 100;
        } else {
            friendship = 0;
        }
    }
    // if object is printed then it will return nama
    public String toString() {
        return nama;
    }
    // getters and setters
    public String getTipe() {
        return tipe;
    }
    public void setFriendship(int amount) {
        friendship += amount;
    }
    public int getJumlahSapa() {
        return jumlahSapa;
    }
    public int getFriendship() {
        return friendship;
    }
}