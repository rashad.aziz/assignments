package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    // class attributes declaration

    private MataKuliah mataKuliah;

    // constructor
    public Dosen(String nama) {
        this.nama = nama;
        this.tipe = "Dosen";
    }
    // method to assign course to dosen
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        // if the dosen has an existing course
        if (this.mataKuliah != null) {
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this, this.mataKuliah);
        } // if the course already has an existing dosen
        else if (mataKuliah.hasDosen()) {
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
        } else {
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.printf("%s mengajar mata kuliah %s\n", this, mataKuliah);
        }
    }
    // method to unassign the dosen from the course
    public void dropMataKuliah() {
        if (mataKuliah == null) {
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this);
        } else {
            System.out.printf("%s berhenti mengajar %s\n", this, this.mataKuliah);
            mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }
    // getters and setters
    public String toString() {
        return nama;
    }

    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }
}