package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    
    // class attributes declaration

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    private int jumlahMatkul;

    // constructor
    public Mahasiswa(String nama, long npm) {
        this.nama = nama;
        this.npm = npm;
        this.jurusan = extractJurusan(npm);
        this.tanggalLahir = extractTanggalLahir(npm);
        this.tipe = "Mahasiswa";
    }
    // method to add matkul
    public void addMatkul(MataKuliah mataKuliah) {
        daftarMataKuliah[jumlahMatkul] = mataKuliah;
        jumlahMatkul++;
    }
    // method to drop matkul
    public void dropMatkul(MataKuliah mataKuliah) {
        int index = 0, length = daftarMataKuliah.length;
        // find index of course
        for (int i = 0; i < length; i++) {
            if (daftarMataKuliah[i] == mataKuliah) {
                index = i;
                break;
            }
        }
        // shift all elements starting from the index
        for (int j = index; j < length-1; j++) {
            daftarMataKuliah[j] = daftarMataKuliah[j+1];
        }
        // set the last element to null if == to second to last element
        if (daftarMataKuliah[length-1] == daftarMataKuliah[length-2]) {
            daftarMataKuliah[length-1] = null;
        }
        jumlahMatkul--;
    }
    // verify whether the student has the matkul passed through the parameter
    public boolean hasMatkul(MataKuliah mataKuliah) {
        boolean has = false;
        for (MataKuliah matkul: daftarMataKuliah) {
            if (matkul != null) {
                if (matkul == mataKuliah) {
                    has = true;
                    break;
                }
            }
        }
        return has;
    }
    // extract date of birth from npm
    private String extractTanggalLahir(long npm) {
        int year = (int) ((npm/100) % 10000);
        int month = (int) ((npm/1000000) % 100);
        int day = (int) ((npm/100000000) % 100);

        return String.format("%d-%d-%d", day, month,
                year);
    }
    // extract major from npm
    private String extractJurusan(long npm) {
        int majorCode = (int) ((npm/100000)/100000) % 100;
        return switch(majorCode) {
            case 1 -> "Ilmu Komputer";
            case 2 -> "Sistem Informasi";
            default -> "";
        };
    }
    // getters and setters
    public String toString() {
        return nama;
    }
    public String getTanggalLahir() {
        return tanggalLahir;
    }
    public String getJurusan() {
        return jurusan;
    }
    public MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }
    public int getJumlahMatkul() {
        return jumlahMatkul;
    }
}